#!/usr/bin/env python3

# Licence:

# Usage: 

import sys
import argparse
import subprocess
from os.path import basename
from subprocess import (
    Popen,
    PIPE
)
import logging as log


class UnknownFileExtension(Exception):
    pass


parser = argparse.ArgumentParser(
    prog=sys.argv[0],
    description="Run one or several fastqc analyses and summarise the result with multiqc",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)

parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Be more verbose."
)
parser.add_argument(
        "-i",
        "--input-files",
        nargs="*",
        required=True,
        help="One or several fastq files."
)
parser.add_argument(
        "-o",
        "--output",
        required=True,
        help="Name of the output directory."

)
parser.add_argument(
        "-j",
        "--cores",
        default=1,
        help="Number of CPU cores to use."
)

args = parser.parse_args()

def run_Popen(process_args):
	process = Popen(process_args, stdout=PIPE, stderr=PIPE)
	(stdout, stderr) = process.communicate()
	if stdout:
		log.info(stdout.decode())
	if stderr:
		log.error(stderr)


def main():
    # Initialise log file
    logfile = args.output + '/analysis.log'
    log.basicConfig(filename=logfile,
                    format='%(asctime)s %(message)s',
                    level=log.DEBUG)
    log.info("Analysis started.")

    # Parse the input file(s) and analyse them one at the time.
    for input_file in args.input_files[0].split(","):
        try:
            process_args = [
                    "fastqc", 
                    input_file,
                    "-o", 
                    args.output ]

            run_Popen(process_args)

        except:
             log.warning("Could not analyse {basename(input_file)}")

	# Run the MultiQC analysis
    process_args = [
            "multiqc",
            args.output, 
            "-o",
            args.output ]

    run_Popen(process_args)

    log.info("Analysis finished.")


if __name__ == "__main__":
    main()
