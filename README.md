This repository creates a docker image that can be used to run cutadapt analyses on fastq files.

# Dependencies
* Docker

# Download from Dockerhub
You can download the docker image from docker hub if you want to run this analysis locally.
```
Add command for downloading from Dockerhub
```

# Build from source
You can also build from source if you want to make changes to the docker image.
```
git clone git@gitlab.com:sequenceit/multiqc-analysis.git
cd multiqc-analysis
docker build --tag multiqc:dev .
```

# Using the docker image
The docker image expects to find the fastq input files to analyse in the directory `input` in this repository. The result of the analysis will end up in the `output` directory.

```
docker run --mount type=bind,source=`pwd`/input/,destination=/media/input --mount type=bind,source=`pwd`/output/,destination=/media/output multiqc-analysis:dev --input /media/input/danube-04_S328_L001_R1_001.fastq.gz /media/input/danube-04_S328_L001_R2_001.fastq.gz --output /media/output
```

# Interactive session
```
docker run -it --entrypoint /bin/bash --mount type=bind,source=`pwd`/input/,destination=/media/input --mount type=bind,source=`pwd`/output/,destination=/media/output multiqc-analysis:dev
```

# Build and push image to Google cloud
```
docker build --tag=sequenceit/multiqc:v0.1.5 .
docker tag sequenceit/multiqc:v0.1.5 eu.gcr.io/sequenceit-207107/multiqc:v0.1.5
gcloud docker -- push eu.gcr.io/sequenceit-207107/multiqc:v0.1.5
```
