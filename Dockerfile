FROM ubuntu:20.04

# Env
ENV TERM=xterm-256color
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Stockholm

# Defaults
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y git
RUN apt-get install -y wget
RUN apt-get install -y unzip
RUN apt-get install -y libcurl4-gnutls-dev
RUN apt-get install -y libfreetype6-dev
RUN apt-get install -y pkg-config
RUN apt-get install -y libpng-dev
RUN apt-get install -y libssl-dev
RUN apt-get install -y libcurl4-openssl-dev
RUN apt-get install -y libxml2-dev
RUN apt-get install -y python3-pip

# Dependencies
RUN apt-get install -y default-jre

# WD
WORKDIR /opt/multiqc
RUN mkdir -p /opt/multiqc

# Install fastqc
#RUN git clone --single-branch -b v0.11.7 https://github.com/s-andrews/FastQC.git
#RUN wget https://github.com/s-andrews/FastQC/archive/v0.11.7.tar.gz
RUN wget https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.7.zip
RUN unzip fastqc_v0.11.7.zip
RUN chmod 755 FastQC/fastqc
ENV PATH=$PATH:/opt/multiqc/FastQC


# Install MultiQC
RUN git clone --single-branch -b v1.9 https://github.com/ewels/MultiQC.git
WORKDIR /opt/multiqc/MultiQC
RUN python3 setup.py install
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Mountpoints
RUN mkdir -p /media/input /media/output

# Wrapper
COPY run_multiQC.py /opt/multiqc

WORKDIR /opt/multiqc
ENTRYPOINT ["python3", "/opt/multiqc/run_multiQC.py"]
CMD ["--help"]
